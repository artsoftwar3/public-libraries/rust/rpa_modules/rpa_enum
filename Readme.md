<div align="center">
    <img src="logo.png" height="200">
</div>

<a href="https://crates.io/crates/rpa">![RPA Enum Version](https://img.shields.io/badge/crates.io-v0.1.6-orange.svg?longCache=true)</a>

### Rpa Enum

This library allows the use of enums for structs on projects
 that are using the library [rpa](https://gitlab.com/artsoftwar3/public-libraries/rust).
Basically we allow the mapping of enums as strings on the database. So if we have for example
the enum:

```rust
pub enum UserType {
    PARTNER,
    VISITANT,
    ADMIN
}
```

And then we have an structure that has a field using that as a type Rpa doesn't allow us to
map that enum into strings since diesel doesn't support that. With this library we can do that.

### How to use it

First you need to import this library like this:

```toml
[dependencies.rpa_enum]
version = "0.1.6"
```

Then we only need to use the derive RpaEnum to make things work, so the example above should look like this:

```rust
use rpa_enum::RpaEnum;

#[derive(RpaEnum)]
#[repr(u32)]
pub enum UserType {
    PARTNER,
    VISITANT,
    ADMIN
}
```

As you can see we only need to use ``#[repr(u32)]`` as the representation and then derive RpaEnum using the trait 
from the crate like this ``use rpa_enum::RpaEnum;``

Please visit [rpa](https://gitlab.com/artsoftwar3/public-libraries/rust) for more information on how rpa works.