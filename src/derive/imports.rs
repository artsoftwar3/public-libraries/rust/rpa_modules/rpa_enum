use quote::quote;

pub fn get_impl() -> proc_macro2::TokenStream {
    quote! {
        use core::fmt;
        use diesel::mysql::Mysql;
        use diesel::sql_types::Text;
        use diesel::deserialize::FromSqlRow;
        use diesel::row::Row;
        use diesel::expression::AsExpression;
        use diesel::dsl::*;
        use serde::{Serialize, Deserialize};
        use diesel::deserialize::FromSql;
        use diesel::backend::Backend;
        use std::error::Error;
        use std::io::Read;
    }
}