use std::collections::HashMap;
use proc_macro::{TokenStream};
use proc_macro2::Span;
use quote::quote;
use syn::Ident;

mod imports;
mod from_sql;
mod display;
mod from_sql_row;
mod as_expression;
mod queryable;

pub fn get_impl(enum_ident: &syn::Ident, variants: Vec<String>, attributes: HashMap<String, Vec<String>>) -> TokenStream {

    let mut optional_variant: Option<Ident> = None;

    if attributes.get("is_optional").is_some() {
        let is_optional_value = attributes.get("is_optional").unwrap().get(0).unwrap().as_str();
        optional_variant = Some(Ident::new(is_optional_value, Span::call_site()));
    }

    let mut variant_idents: HashMap<String, Ident> = HashMap::new();

    variants.iter().for_each(|variant| {
        let variant_ident = Ident::new(variant.as_str(), Span::call_site());
        variant_idents.insert(variant.to_string(), variant_ident);
    });

    let imports = imports::get_impl();
    let from_sql = from_sql::get_impl(enum_ident, &variant_idents, &optional_variant);
    let display = display::get_impl(enum_ident, &variant_idents);
    let from_sql_row = from_sql_row::get_impl(enum_ident, &variant_idents);
    let as_expression = as_expression::get_impl(enum_ident);
    let queryable = queryable::get_impl(enum_ident, &variant_idents);

    let code_generated = quote! {
        #imports
        #from_sql
        #display
        #from_sql_row
        #as_expression
        #queryable
    };

    code_generated.into()
}