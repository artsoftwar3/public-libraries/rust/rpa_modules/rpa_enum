use quote::quote;

pub fn get_impl(enum_ident: &syn::Ident) -> proc_macro2::TokenStream {
    return quote! {
        impl AsExpression<Text> for #enum_ident {
            type Expression = AsExprOf<String, Text>;
            fn as_expression(self) -> Self::Expression {
                <String as AsExpression<Text>>::as_expression(self.to_string())
            }
        }

        impl<'a> AsExpression<Text> for &'a #enum_ident {
            type Expression = AsExprOf<String, Text>;
            fn as_expression(self) -> Self::Expression {
                <String as AsExpression<Text>>::as_expression(self.to_string())
            }
        }
    };
}