use quote::quote;
use std::collections::HashMap;

pub fn get_impl(enum_ident: &syn::Ident, variant_idents: &HashMap<String, syn::Ident>) -> proc_macro2::TokenStream {

    let mut variants: Vec<proc_macro2::TokenStream> = Vec::new();

    variant_idents.keys().for_each(|variant_name| {
        let variant_ident = variant_idents.get(variant_name).unwrap();
        let variant_template = quote! {
             #enum_ident::#variant_ident => #variant_name,
        };
        variants.push(variant_template.into());
    });

    return quote! {
        impl fmt::Display for #enum_ident {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                write!(f, "{}", match *self {
                    #(#variants)*
                })
            }
        }
    };
}