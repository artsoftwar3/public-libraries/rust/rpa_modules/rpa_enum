use quote::quote;
use std::collections::HashMap;

pub fn get_impl(enum_ident: &syn::Ident, variant_idents: &HashMap<String, syn::Ident>) -> proc_macro2::TokenStream {

    let mut variants: Vec<proc_macro2::TokenStream> = Vec::new();

    variant_idents.keys().for_each(|variant_name| {
        let variant_ident = variant_idents.get(variant_name).unwrap();
        let variant_template = quote! {
            #variant_name => Ok(#enum_ident::#variant_ident),
        };
        variants.push(variant_template.into());
    });

    return quote! {
        impl FromSqlRow<Text, Mysql> for #enum_ident {
            fn build_from_row<R: Row<Mysql>>(row: &mut R) -> Result<Self, Box<dyn Error+Send+Sync>> {
                match String::build_from_row(row)?.as_ref() {
                    #(#variants)*
                    v => Err(format!("Unknown value {} for {} found", stringify!(#enum_ident), v).into()),
                }
            }
        }
    };
}