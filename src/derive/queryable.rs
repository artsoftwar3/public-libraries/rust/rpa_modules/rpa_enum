use quote::quote;
use std::collections::HashMap;

pub fn get_impl(enum_ident: &syn::Ident, variant_idents: &HashMap<String, syn::Ident>) -> proc_macro2::TokenStream {

    let mut variants: Vec<proc_macro2::TokenStream> = Vec::new();

    variant_idents.keys().for_each(|variant_name| {
        let variant_ident = variant_idents.get(variant_name).unwrap();
        let variant_template = quote! {
             #variant_name => #enum_ident::#variant_ident,
        };
        variants.push(variant_template.into());
    });

    return quote! {
        impl diesel::Queryable<diesel::sql_types::Text, diesel::mysql::Mysql> for #enum_ident {
            type Row = String;

            fn build(row: Self::Row) -> Self {
                match row.as_str() {
                    #(#variants)*
                    wrong_value => panic!("Wrong value for {}: {}", stringify!(#enum_ident), wrong_value)
                }
            }
        }
    };
}