#![recursion_limit="256"]
#![feature(proc_macro_hygiene, decl_macro)]

/**
    Rpa (Rust Persistence API) Enum Library
    Copyright (C) 2019  Jonathan Franco

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

extern crate proc_macro;
extern crate syn;
extern crate inflector;

use crate::proc_macro::TokenStream;
use generic::derive_macro;

mod generic;
mod derive;

#[proc_macro_derive(RpaEnum, attributes(is_optional))]
pub fn derive_crud(tokens: TokenStream) -> TokenStream {
    derive_macro(tokens, derive::get_impl)
}
